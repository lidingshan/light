class Refresh < ActiveRecord::Base
  belongs_to :server
  belongs_to :application
  belongs_to :status
  belongs_to :source, :class_name => 'Server', :foreign_key => 'source_id'
  attr_accessible :eod_date, :implement_time
end
