class Deployment < ActiveRecord::Base
    attr_accessible :date, :deployed_by
    belongs_to :server
    belongs_to :status
    has_and_belongs_to_many :packages
end
