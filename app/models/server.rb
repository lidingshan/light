class Server < ActiveRecord::Base
    attr_accessible :host, :ip, :name, :is_benchmark
    attr_accessor :applications

    has_and_belongs_to_many :packages
    has_many :deployments, :after_add => :on_deployment_add

    after_initialize :init_applications

    def init_applications
        @applications = {}
        packages.each { |pkg|
            app = pkg.application
            add_application(app)
        }
    end


    def add_application(application)
        if !@applications.has_key?(application.name)
            @applications[application.name] = application
        end
    end

    def find_application(app)
        begin
            return @applications[app.name]
        rescue
            return nil 
        end
    end

    def add_package(package)
        if !packages.exists?(package)
            packages<<package
            add_application(package.application)
        end
    end

    def clone_application(app_name, src_server, before_date = nil)
        packages.each{ |pkg|
            if pkg.application.name == app_name
                if !before_date.nil? 
                    pkg.clean_deployments_after_date(self, before_date)
                end
                packages.delete(pkg)
            end
        }

        src_server.packages.each{ |pkg|
            if pkg.application.name == app_name
                if (before_date.nil?)
                    packages << pkg
                else
                    pkg.deployments.each{ |deployment|
                        if(pkg.installed_before_date?(src_server, before_date))            
                            packages << pkg
                        end
                    }                    
                end
            end
        }
    end

    private
    def on_deployment_add(deploy)
        deploy.packages.each{ |package|
            add_package(package)
        }
    end

end
