class Package < ActiveRecord::Base
    attr_accessible :date, :deployed_by, :name

    belongs_to :application
    has_and_belongs_to_many :servers
    has_and_belongs_to_many :deployments

    def installed?(server)
        is_installed = false
        deployments.each{ |deploy|
            if deploy.server.name == server.name
                is_installed = true
            end
        }

        is_installed
    end

    def installed_before_date?(server, before_date)
        is_installed = false
        deployments.each{ |deploy|
            deploy_date = Time.new(deploy.date.year, deploy.date.month, deploy.date.day)
            if(deploy.server.name == server.name and deploy_date <= before_date)
                is_installed = true
            end
        }

        is_installed
    end

    def clean_deployments_after_date(server, date)
        deployments.each{ |deploy|
            deploy_date = Time.new(deploy.date.year, deploy.date.month, deploy.date.day)
            if deploy.server == server and deploy_date > date
                deployments.delete(deploy)
                deploy.destroy
            end
        }
        save
    end

end
:wa
