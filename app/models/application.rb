class Application < ActiveRecord::Base
    has_many :packages
    attr_accessible :name 

    belongs_to :production, :class_name => 'Server', :foreign_key => 'production_id'
    
end
