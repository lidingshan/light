class PackageController < ApplicationController

    def index
        @packages = Package.all
        @servers = Server.all
        render 
    end

    def show
        id = params[:id].to_i
        @package = Package.find_by_id(id) 
        render
    end

    def new
        @package = Package.new
        @applications = Application.all
        render
    end

    def create
        package = params[:package]
        
        new_package = Package.new
        new_package.name = package[:name]
        new_package.application = Application.find_by_id(package[:application_id].to_i)
        new_package.save

        redirect_to :action => 'index'
    end
end
