class ApplicationController < ActionController::Base

    def index
        @applications = Application.all
        render
    end

    def new
        @application = Application.new
        @prod_servers = Server.find(:all, :conditions => 'is_benchmark = true')
        render
    end

    def create
        application = params[:application]
        production = Server.find_by_id(application[:production_id].to_i)
        new_application = Application.new(:name => application[:name])
        new_application.production = production
        new_application.save

        redirect_to :action => 'index'
    end
end
