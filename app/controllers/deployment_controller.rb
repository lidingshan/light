class DeploymentController < ApplicationController

    def index
        @deployments = Deployment.all
        render
    end

    def show
        id = params[:id].to_i

        @deploy = Deployment.find_by_id(id)
        render 
    end 

    def new
        @deployment = Deployment.new
        @deployment.status = Status.find_by_name('Open')
        @servers = Server.all
        @statuses = Status.all
        @packages = Package.all

        render
    end

    def create
        deployment = params[:deployment]
        year = deployment['date(1i)'].to_i
        month = deployment['date(2i)'].to_i
        date = deployment['date(3i)'].to_i
        hour = deployment['date(4i)'].to_i
        minute = deployment['date(5i)'].to_i

        deployed_time = Time.new(year, month, date, hour, minute)

        new_deployment = Deployment.new
        new_deployment.date = deployed_time
        new_deployment.deployed_by = deployment[:deployed_by]
        new_deployment.server = Server.find_by_id(deployment[:server_id].to_i)
        new_deployment.status = Status.find_by_id(deployment[:status_id].to_i)
        
        deployment[:packages].each{ |id| 
            pkg_id = id.to_i
            if pkg_id > 0
                new_deployment.packages << Package.find_by_id(pkg_id)
            end
        }

        new_deployment.save 

        redirect_to :action => 'index'
    end

    def close
        id = params[:id].to_i
        deploy = Deployment.find_by_id(id)

        deploy.server.deployments << deploy
        deploy.server.save

        deploy.status = Status.find_by_name("Closed")
        deploy.save

        render :json => {:id=>deploy.id, :status=>deploy.status }.to_json 
    end
end
