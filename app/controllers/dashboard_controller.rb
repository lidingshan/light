class DashboardController < ApplicationController
    def index
        @prod_servers = Server.where('is_benchmark=true')
        @non_prod_servers = Server.where('is_benchmark=false')

        @coming_deployments = Deployment.find(:all, 
                                              :conditions => 'date>="%s"' % Time.now.strftime("%Y-%m-%d"),
                                              :order => 'date',
                                              :limit => 5)

        @coming_refreshments = Refresh.find(:all, 
                                            :conditions => 'implement_time>="%s"' % Time.now.strftime("%Y-%m-%d"),
                                            :order => 'implement_time',
                                            :limit => 5)
        render
    end

    def new_server
        @server = Server.new
        render
    end

    def create_server
        server = params[:server]
        is_benchmark = server[:is_benchmark] == '1'

        Server.create(
            :name=>server[:name],
            :host=>server[:host],
            :ip=>server[:ip],
            :is_benchmark=>is_benchmark)

        redirect_to action: "index"
    end
end
