class RefreshController < ApplicationController
    def index
        @refreshes = Refresh.all
        render 
    end

    def new
        @refresh = Refresh.new
        @refresh.status = Status.find_by_name('Open')

        @servers = Server.all
        @statuses = Status.all
        @applications = Application.all

        render
    end

    def create
        refresh = params[:refresh]

        eod_year = refresh['eod_date(1i)'].to_i
        eod_month = refresh['eod_date(2i)'].to_i
        eod_day = refresh['eod_date(3i)'].to_i

        eod_date = Time.new(eod_year, eod_month, eod_day)

        impl_year = refresh['implement_time(1i)'].to_i
        impl_month = refresh['implement_time(2i)'].to_i
        impl_day = refresh['implement_time(3i)'].to_i
        impl_hour = refresh['implement_time(4i)'].to_i
        impl_minute = refresh['implement_time(5i)'].to_i

        implement_time = Time.new(impl_year, impl_month, impl_day, impl_hour, impl_minute)

        new_refresh = Refresh.new
        new_refresh.eod_date = eod_date
        new_refresh.implement_time = implement_time
        new_refresh.source = Server.find_by_id(refresh[:source_id].to_i)
        new_refresh.server = Server.find_by_id(refresh[:server_id].to_i)
        new_refresh.application = Application.find_by_id(refresh[:application_id].to_i)
        new_refresh.status = Status.find_by_id(refresh[:status_id].to_i)
        new_refresh.save

        redirect_to :action => 'index'
    end

    def complete
        id = params[:id].to_i
        refresh = Refresh.find_by_id(id)
        refresh.server.clone_application(refresh.application.name, refresh.source, refresh.eod_date)
        refresh.server.save

        refresh.status = Status.find_by_name("Closed")
        refresh.save

        render :json => {:id=>refresh.id, :server=>refresh.server, :application=>refresh.application, :eod=>refresh.eod_date, :status=>refresh.status, :implement_time=>refresh.implement_time }.to_json 

    end
end
