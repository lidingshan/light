class VersionDiff
    attr_accessor :missed_packages, :additional_packages, :is_same

    def initialize
        @missed_packages = [] 
        @additional_packages = []
        @is_same = true
    end
    
    def compare(application, source, target)
        find_additional_packages(application, source, target)
        find_missed_packages(application, source, target)
        @is_same = (@additional_packages.length == 0 and @missed_packages.length == 0)
    end

    private
    def find_additional_packages(application, source, target)
        @additional_packages = find_undeployed_packages_on_dest_server(application, source, target)
    end

    def find_missed_packages(application, source, target)
        @missed_packages = find_undeployed_packages_on_dest_server(application, target, source)
    end

    def find_undeployed_packages_on_dest_server(app, src, dest)
        undeployed_pkgs = []

        src.packages.each{ |pkg|
            if !dest.packages.exists?(pkg) and pkg.application.name == app.name
                undeployed_pkgs<<pkg
            end
        }

        undeployed_pkgs
    end
end
