function onDeploymentCloseButtonClick(deploy_id) {
    url = "/deployment/close/" + deploy_id
    $.post(url, function(data) {
        deploy = data;
        element_id = "#status_cell_" + deploy.id;
        $(element_id).html(deploy.status.name);
    }); 
}
