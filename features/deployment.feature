Feature: Deployment Management
    In order to have high visibility of deployment status
    As I user
    I want to be able to view the deployment information of each package on each server

    Background:
        Given the servers and application and packages have been set up
        And there are two deployment requests
            | server | package | date | deployed_by |
            | UAT | PKG_0001 | 2013-08-01 | Laksh |
            | SIT | PKG_0001 | 2013-08-02 | Laksh |

    Scenario: Display release info of one package on each server
        When I open deployment information page
        Then the deployment information list should be displayed

    Scenario: Display one deployment detail information
        When I access "/deployment/get/{id}"
        Then the page should display the detail information of the deployment

    Scenario: Complete deployment request
        When I select to complete the first deployment request
        Then the package "PKG_0001" should be installed on target server


        
