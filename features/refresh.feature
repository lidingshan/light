Feature: Refresh Management
    In order to synchronize the packages of the server from production
    As I user
    I want to be able to schedule and execute refreshment for an application on one server

    Background:
        Given the servers and application and packages have been set up
        And the status values include
            | name |
            | Open |
            | Closed|
        And the packages have been deployed on servers
            | server | package | date | deployed_by |
            | Prod | PKG_0001 | 2013-08-01 | Laksh |
            | Prod | PKG_0002 | 2013-08-03 | Laksh |
        And there are two scheduled refresh request
            | server | application | source | eod_date | status | implement_time |
            | UAT | T24 | Prod | 2013-08-03 | Open | 2013-08-02 09:00:00 |
            | SIT | T24 | Prod | 2013-08-02 | Open | 2013-08-02 10:00:00 |
        
    Scenario: Display scheduled refresh request on each server
        When I open refresh index page
        Then the scheduled refresh list should be displayed

    Scenario: Trigger refreshment to complete the request
        When I request to complete the first refresh request 
        And I open refresh index page
        Then the status of the request should changed to be "Closed"
        And the application's packages on the server should include "PKG_0001,PKG_0002" 
