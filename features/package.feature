Feature: Package Information
    In order to know the detail inforamtion of a package
    As a user
    I want to see the package information

    Scenario: Display package basic information
        Given the following package is existing
            | name | application |
            | PKG_0001 | T24 |
        When I access package page with id of PKG_0001
        Then the package information should be displayed

    Scenario: Display package deployment information
        Given the servers and application and packages have been set up
        And the packages have been deployed on all boxes
        When I access package page with id of PKG_0001
        Then the package deployment information should be displayed

    Scenario: Display package list
        Given the servers and application and packages have been set up
        And the packages have been deployed on all boxes
        When I access package list page
        Then the list of packages should be displayed 
        
