Feature: Dashboard
    In order to know the status of each staging box
    As a developer
    I want to be able to have a view to indicate the difference of each application's verion of each staging box
    
    Background:
        Given the servers and application and packages have been set up
        And the packages have been deployed on all boxes

    Scenario: Display all of the boxes
        When I open the dashboard page
        Then it should display boxes with the server information

    Scenario: Display applications information in all boxes
        Given I open the dashboard page
        Then it should include all applications information in each box

    Scenario: Display same deployed application package information in all boxes
        Given I open the dashboard page
        Then it should indicate application version are same on each box

    Scenario: Display additional package between test environment and product environment
        Given I deployed the following packages on UAT
            | name | application |
            | PKG_0004 | T24 |
        When I open the dashboard page
        Then UAT box should display one '+' to indicate it has the above new deployed package
        And it should include the hyperlink to the additional package information

    Scenario: Display missed package between test environment and product environment
        Given I deployed the following packages on Prod 
            | name | application |
            | PKG_0004 | T24 |
        When I open the dashboard page
        Then UAT box should display one '-' to indicate it hasn't deployed above package
        And it should include the hyperlink to the missed package information
