Feature: This is a common steps definition so we may reuse them in different feature files

    Scenario: Prepare data
        Given I have different environment boxes like bellow
            | name | host | ip | 
            | SIT | SITServer | 10.0.0.1 |
            | UAT | UATServer | 10.0.0.2|
            | Pre Prod | PreProdServer | 10.0.0.3|
            | Prod | Production | 10.0.1.4 |
        And is supporting the following applications
            | name |
            | T24 |
            | Fiscal |
        And there are following packages can be deployed
            | name | application |
            | PKG_0001 | T24 | 
            | PKG_0002 | T24 |
            | PKG_0003 | Fiscal |
