class StepHelper

    def initialize
        @packages = []
        @applications = []
    end

    def create_servers(servers)
        @servers = Server.create(servers.hashes)
    end

    def servers
        @servers
    end

    def create_package(hash)
        application = Application.find_by_name(hash['application'])
        if application.nil?
            application = Application.new(:name=>hash['application'])
        end
        package = Package.new(:name=>hash['name'])

        application.packages<<package
        application.save

        package.application = application
        package.save
        @packages << package

        package
    end

    def create_application(app, prod_server)
        application = Application.new
        application.name = app['name']
        application.production = prod_server
        application.save

        @applications << application
    end

    def deploy_packages_on_server
        @servers.each{ |server|
            deploy = Deployment.new(:date=>'2013-9-1', :deployed_by=>'Tester1')
            deploy.server = server

            @packages.each{ |pkg|
                deploy.packages<<pkg
            }

            deploy.save
            server.deployments<<deploy
        }
    end

    def compose_url(url, params)
        if !params.nil?
            params.each_key{ |key|
                value = params[key]
                url = url.sub(/#{key}/, value)
            }
        end

        url
    end
end
