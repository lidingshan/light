
And(/^the packages have been deployed on servers$/) do |deployments|
    
    _status = Status.find_by_name('Closed')

    deployments.hashes.each{ |hash|
        server = Server.find_by_name(hash["server"])
        package = Package.find_by_name(hash["package"])

        deploy = Deployment.new(
            :date=>hash["date"],
            :deployed_by=>hash["deployed_by"]
        )

        deploy.status = _status
        deploy.packages << package
        deploy.server = server
        deploy.save

        server.deployments << deploy
        server.save
    }
end

And(/^the status values include$/) do |status_names|
    status_names.hashes.each{ |row|
        Status.create(:name=>row['name'])
    }
end

And(/^there are two scheduled refresh request$/) do |refresh_list|
    @refresh_requests = refresh_list
    @refresh_ids = []

    refresh_list.hashes.each{ |row|
        refresh = Refresh.new
        refresh.server = Server.find_by_name(row['server'])
        refresh.application = Application.find_by_name(row['application'])
        refresh.source = Server.find_by_name(row['source'])
        refresh.status = Status.find_by_name(row['status'])
        refresh.eod_date = row['eod_date']
        refresh.implement_time = row['implement_time']
        refresh.save

        @refresh_ids<<refresh.id
    }

    expect(Refresh.all.size).to eq(2)
end

When(/^I open refresh index page$/) do
    visit '/refresh'
end

Then(/^the scheduled refresh list should be displayed$/) do
    @refresh_requests.hashes.each{ |req|
        should have_xpath('//table/tbody/tr/td[contains(.,"%s")]' % req['server'])
        should have_xpath('//table/tbody/tr/td[contains(.,"%s")]' % req['application'])
        should have_xpath('//table/tbody/tr/td[contains(.,"%s")]' % req['status'])
        should have_xpath('//table/tbody/tr/td[contains(.,"%s")]' % req['eod_date'])
        should have_xpath('//table/tbody/tr/td[contains(.,"%s")]' % req['implement_time'])
    }
end

When(/^I request to complete the first refresh request$/) do
    page.driver.post('/refresh/close/%d' % @refresh_ids[0])
end 

Then(/^the status of the request should changed to be "(.*?)"$/) do |status_name|
    expect(find_by_id("status_cell_%d" % @refresh_ids[0]).text).to eq(status_name)
end

Then(/^the application's packages on the server should include "(.+)"$/) do | package_name |
    
    expected_names = package_name.split(",")

    refresh = Refresh.find_by_id(@refresh_ids[0])
    server = refresh.server
    expect(server.packages.size).to eq(2)
    expect(server.packages[0].name).to eq(expected_names[0])    
    expect(server.packages[1].name).to eq(expected_names[1])    
end
