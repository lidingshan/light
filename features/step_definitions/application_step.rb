
When(/^I open application list page$/) do
    visit '/application'
end

Then(/^the application list should be displayed$/) do
    page.should have_content('T24')
    page.should have_content('Fiscal')
end
