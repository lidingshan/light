require 'rspec-rails'

And(/^the packages have been deployed on all boxes$/) do
    @helper.deploy_packages_on_server
end

When(/^I open the dashboard page$/) do 
    @dashboard_page = DashboardPage.new
    @dashboard_page.open
end

Then(/^it should display boxes with the server information$/) do
    @helper.servers.each { |server|
        page.should have_content(server.name)
        page.should have_content(server.host)
        page.should have_content(server.ip)
    }
end

Then(/^it should include all applications information in each box$/) do
    @app_divs = all(:xpath, "//div[contains(text(), 'T24')]")
    expect(@app_divs.length).to eq(@helper.servers.length)

    @app_divs = all(:xpath, "//div[contains(text(), 'Fiscal')]")
    expect(@app_divs.length).to eq(@helper.servers.length)
end


Then(/^it should indicate application version are same on each box$/) do
    @pkg_divs = all('div.div_pkg')
    expect(@pkg_divs.length).to eq(8)

    @pkg_divs.each { |div|
        expect(div.text).to eq('no diff')
    }
end

Given(/^I deployed the following packages on (.+)$/) do | env_name, packages |
    env = Server.find_by_name(env_name)

packages.hashes.each { | pkg |
    package = @helper.create_package(pkg)
@pkg_id = package.id

env.add_package(package)
env.save
}

end

Then(/^(.+) box should display one '(.+)' to indicate it has the above new deployed package$/) do | env_name, symbol |
    check_package_difference_symbol_in_server_div(env_name, symbol)
end

And(/^it should include the hyperlink to the additional package information$/) do
    pkg_link = find_link('+')
    expect(pkg_link[:href]).to eq('/package/get/%d'% @pkg_id)
end

Then(/^(.+) box should display one '(.+)' to indicate it hasn't deployed above package$/) do | env_name, symbol |
    check_package_difference_symbol_in_server_div(env_name, symbol)
end

And(/^it should include the hyperlink to the missed package information$/) do
    xpath_enquiry = '//div[@id="%s"]/div/div[@class="div_pkg"]/a' % @div_id
    pkg_link = all(:xpath, xpath_enquiry).first

    expect(pkg_link[:href]).to eq('/package/get/%d'% @pkg_id)
end

private
def check_package_difference_symbol_in_server_div(server_name, symbol)
    @div_id = server_name
    xpath_enquiry = '//div[@id="%s"]/div/div[@class="div_pkg"]' % @div_id
    pkg_div = all(:xpath, xpath_enquiry).first

    expect(pkg_div.text).to eq(symbol)
end
