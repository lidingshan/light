
Given(/^the following package is existing$/) do |package|
    StepHelper.create_package(package.hashes[0])
end

When(/^I access package page with id of (.+)$/) do |pkg_name|
    @package = Package.find_by_name(pkg_name)
    visit '/package/get/%d' % @package.id
end

Then(/^the package information should be displayed$/) do
    page.should have_content(@package.name)
    page.should have_content(@package.application.name)
end

Then(/^the package deployment information should be displayed$/) do
    expect(@package.deployments.length).to eq(4)

    @package.deployments.each{ |deploy|
        page.should have_content(deploy.server.name)
        page.should have_content(deploy.date)
        page.should have_content(deploy.deployed_by)
    }
end

When(/^I access package list page$/) do
    visit '/package' 
end

Then(/^the list of packages should be displayed$/) do
    packages = Package.all
    packages.each{ |package| 
        page.should have_content(package.name)
        page.should have_content(package.application.name)
    }
end
