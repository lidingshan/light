require "capybara"

class DashboardPage
    include Capybara::DSL

    def open
        visit '/dashboard'
    end

end
