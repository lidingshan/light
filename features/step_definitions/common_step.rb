require 'rspec-rails'

Given(/^the servers and application and packages have been set up$/) do
    steps %{
        Given I have different environment boxes like bellow
            | name | host | ip | 
            | SIT | SITServer | 10.0.0.1 |
            | UAT | UATServer | 10.0.0.2|
            | Pre Prod | PreProdServer | 10.0.0.3|
            | Prod | Production | 10.0.1.4 |
        And is supporting the following applications
            | name |
            | T24 |
            | Fiscal |
        And there are following packages can be deployed
            | name | application |
            | PKG_0001 | T24 | 
            | PKG_0002 | T24 |
            | PKG_0003 | Fiscal |
    }
end

Given(/^I have different environment boxes like bellow$/) do |servers|
    @helper = StepHelper.new
    @helper.create_servers(servers)
end

And(/^is supporting the following applications$/) do |apps|
    prod_server = Server.find_by_name("Prod")
    apps.hashes.each{ |app|
        @helper.create_application(app, prod_server)
    }
end

And(/^there are following packages can be deployed$/) do |packages|
    packages.hashes.each { |package|
        @helper.create_package(package)
    }
end

