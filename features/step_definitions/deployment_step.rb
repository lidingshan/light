And(/^there are two deployment requests$/) do | deployments |
    @deploy_list = []
    status_open = Status.create(:name=>'Open')

    deployments.hashes.each{ |hash|
        server = Server.find_by_name(hash["server"])
        package = Package.find_by_name(hash["package"])

        deploy = Deployment.new(
            :date=>hash["date"],
            :deployed_by=>hash["deployed_by"]
        )

        deploy.status = status_open
        deploy.packages << package
        deploy.server = server
        deploy.save

        @deploy_list << deploy
    }
end

When(/^I open deployment information page$/) do
    visit '/deployment'
end

Then(/^the deployment information list should be displayed$/) do
    page_should_include_deployment_information @deploy_list
end

When(/^I access "(.*?)"$/) do |url|
    id = @deploy_list[0].id
    url = url.sub(/{id}/, id.to_s)
    visit url
end

Then(/^the page should display the detail information of the deployment$/) do
    page_should_include_deployment_information [@deploy_list[0]]
end

When(/^I select to complete the first deployment request$/) do
    post_url = '/deployment/close/%d' % @deploy_list[0].id
    page.driver.post(post_url)
end

Then(/^the package "(.*?)" should be installed on target server$/) do |package_name|
    server = @deploy_list[0].server
    package = Package.find_by_name(package_name)
    expect(package.installed?(server)).to be_true
end

def page_should_include_deployment_information(expected_deployments)
    expected_deployments.each{ |deploy|
        page.should have_content(deploy.date)
        page.should have_content(deploy.server.name)
        page.should have_content(deploy.packages[0].name)
        page.should have_content(deploy.deployed_by)
    }
end
