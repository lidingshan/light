class TestHelper
    def self.deploy(date, by, package, server)
        deploy = Deployment.new(
            :date => date,
            :deployed_by => by
        )

        deploy.packages<<package
        package.deployments<<deploy

        server.deployments<<deploy

        deploy.save
        package.save
        server.save
    end
end
