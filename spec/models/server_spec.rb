require 'spec_helper'

describe Server do
    fixtures :servers, :applications, :packages

    it 'should get all servers' do
        expect(Server.count).to eq(4)
    end

    it 'should include empty application list after created'do
        sit = servers(:SIT)
        expect(sit.applications.length).to eq(0)
    end

    it 'should be able to get application on by one' do
        pkg1 = packages(:Package1)
        pkg2 = packages(:Package2)

        t24 = applications(:T24)
        fiscal = applications(:Fiscal)

        pkg1.application = t24
        pkg2.application = fiscal

        server = servers(:UAT)
        server.add_package(pkg1)
        server.add_package(pkg2)

        expect(server.applications.length).to eq(2)
        server.applications.each_value { |app|
            expect(app).to be_true
        }
    end

    it 'should be able to add packages' do
        t24 = applications(:T24)
        
        pkg = packages(:Package1)
        pkg.application = t24

        sit = servers(:SIT)
        sit.add_package(pkg)

        expect(sit.packages.size).to eq(1)
    end

    it 'should not allowed to add same package more than once' do
        pkg1 = packages(:Package1)
        t24 = applications(:T24)
        pkg1.application = t24

        pkg1_1 = pkg1.clone

        sit = servers(:SIT)

        sit.add_package(pkg1)
        sit.add_package(pkg1_1)

        expect(sit.packages.size).to eq(1)
    end

    it 'should cache application when add a package' do
        t24 = applications(:T24)
        pkg = packages(:Package1)

        t24.packages<<pkg
        pkg.application = t24

        sit = servers(:SIT)
        sit.add_package(pkg)

        expect(sit.applications[t24.name]).to be_true
    end

    it 'should be able to find existing application' do
        pkg = packages(:Package1)
        t24 = applications(:T24)

        pkg.application = t24
        t24.packages<<pkg

        server = servers(:SIT)
        server.add_package(pkg)

        expect(server.find_application(t24)).to be_true
    end

    it 'should return nothing if try to find application not existed' do
        server = servers(:SIT)
        t24 = applications(:T24)

        expect(server.find_application(t24)).to be_false
    end

    it 'should be able to set a benchmark server for version comparison' do
        server = servers(:T24Prod)
        expect(server.is_benchmark).to be_true
    end

    it 'should be able to relate to multiple deployment' do
        create_deployment_data
        
        server = servers(:UAT)
        server.deployments<<@deploy1
        server.deployments<<@deploy2

        expect(server.deployments.size).to eq(2)
    end

    it 'should add packages to server when apply a deployment' do
        create_deployment_data

        server = servers(:UAT)
        server.deployments<<@deploy1
        server.deployments<<@deploy2

        expect(server.packages.exists?(@deploy1.packages[0])).to be_true
        expect(server.packages.exists?(@deploy2.packages[0])).to be_true
    end

    it 'should be able to clone the whole application from another server' do
        create_deployment_data
        t24 = applications(:T24)

        uat = servers(:UAT)
        prod = servers(:T24Prod)

        uat.deployments<<@deploy1
        prod.deployments<<@deploy2

        uat.clone_application(t24.name, prod)
        expect(uat.packages).to eq(prod.packages)
    end

    private
    def create_deployment_data
        @deploy1 = Deployment.new(
            :date=>'2013-9-1',
            :deployed_by=>'tester1'
        )

        @deploy2 = Deployment.new(
            :date=>'2013-9-2',
            :deployed_by=>'tester2'
        )

        pkg1 = packages(:Package1)
        pkg2 = packages(:Package2)

        pkg1.application = applications(:T24)
        pkg2.application = applications(:T24)

        @deploy1.packages<<pkg1
        @deploy2.packages<<pkg2
    end
    
end
