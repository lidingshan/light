require 'spec_helper'

describe Application do
    fixtures :applications, :packages, :servers

    it 'should not be able to add new application with same name' do
        app1 = Application.new(:name=>'TestApp')
        app1.save

        app2 = Application.new(:name=>'TestApp')
        lambda {app2.save}.should raise_error(ActiveRecord::RecordNotUnique)
    end

    it 'should be able to include multiple packages' do
        t24 = applications(:T24)
       
        pkg1 = packages(:Package1)
        pkg2 = packages(:Package2)

        t24.packages<<pkg1
        t24.packages<<pkg2

        expect(t24.packages.size).to eq(2)
    end

    it 'should include production server' do
        t24 = applications(:T24)
        t24_prod = servers(:T24Prod)

        t24.production = t24_prod
        expect(t24.production).to eq(t24_prod)
    end

    it 'should be able to save production server' do
        t24 = applications(:T24)
        t24_prod = servers(:T24Prod)

        t24.production = t24_prod

        expect(t24.production.id).to eq(t24_prod.id)
    end

    it 'should be able to get production server in initialization' do
        t24 = applications(:T24)
        t24_prod = servers(:T24Prod)
        t24.production = t24_prod
        t24.save

        app = Application.find_by_id(t24.id)
        expect(app.production.id).to eq(t24_prod.id)

    end
end
