require 'spec_helper'
require 'test_helper'

describe Package do
    fixtures :servers, :applications, :packages, :statuses

    before(:each) do
        @pkg = packages(:Package1)
        @app = applications(:T24)

        @pkg.application = @app
        @app.packages<<@pkg

        @server = servers(:UAT) 
    end

    after(:each) do
        @pkg.deployments.destroy_all
        @pkg.save
    end

    it 'should be able to belong to a application' do
        expect(@pkg.application.id).to eq(@app.id)
    end

    it 'should be able to install on a server' do
        TestHelper.deploy('2013-9-1', 'Tester', @pkg, @server)

        expect(@pkg.servers.size).to eq(1)
        expect(@pkg.servers[0].name).to eq(@server.name)
    end

    it 'should be able to tell it has been installed on specific server' do
        TestHelper.deploy('2013-9-1', 'Tester', @pkg, @server)

        expect(@pkg.installed?(@server)).to be_true
    end
    
    it 'should be able to tell it has not been installed on specific server' do
        sit_server = servers(:SIT)
        expect(@pkg.installed?(sit_server)).to be_false
    end

    it 'should be able to clean the relative deployments completed after given date' do
        TestHelper.deploy('2013-9-1', 'Tester', @pkg, @server)
        @pkg.clean_deployments_after_date(@server, Time.new(2013,8,1))
        expect(@pkg.installed?(@server)).to be_false                                      
    end
end
