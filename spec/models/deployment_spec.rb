require 'spec_helper'

describe 'Deployment model' do
    fixtures :packages, :servers

    before(:each) do
        @deployment = Deployment.new(
            :date => '2013-9-1',
            :deployed_by => 'Laksh'
        )

        @deployment.save
    end

    it 'should be able to be saved' do
        actual = Deployment.find_by_id(@deployment.id)
        expect(actual).to be_true
    end

    it 'should belong to one server' do
        @deployment.server = servers(:SIT)
        @deployment.save

        actual = Deployment.find_by_id(@deployment.id)
        expect(actual.server.id).to eq(@deployment.server.id)
    end

    it 'should include multiple packages' do
        pkg1 = packages(:Package1)
        pkg2 = packages(:Package2)

        @deployment.packages << pkg1
        @deployment.packages << pkg2
        @deployment.save

        actual = Deployment.find_by_id(@deployment.id)
        expect(actual.packages.size).to eq(2)
    end
end
