require 'spec_helper'

describe Refresh do
    fixtures :servers, :applications, :statuses

    it 'should be able to set refresh from server' do
        source_server = servers(:SIT)

        refresh = Refresh.new
        refresh.server = servers(:UAT)
        refresh.application = applications(:T24)
        refresh.status = statuses(:Open)
        refresh.source = source_server
        refresh.save

        actual = Refresh.find_by_id(refresh.id)
        
        expect(actual).to be_true
        expect(actual.source).to eq(servers(:SIT))
    end
end
