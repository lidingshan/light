require 'spec_helper'

describe 'VersionDiff' do

    fixtures :servers, :applications, :packages    

    before(:each) do
        @server1 = servers(:UAT)
        @server2 = servers(:T24Prod)

        @t24 = applications(:T24)
        
        pkg1 = packages(:Package1)
        pkg2 = packages(:Package2)
    
        pkg1.application = @t24
        pkg1.save

        pkg2.application = @t24
        pkg2.save

        @server1.add_package(pkg1)
        @server1.add_package(pkg2)
        @server1.save

        @server2.add_package(pkg1)
        @server2.add_package(pkg2)
        @server2.save

        @diff = VersionDiff.new
    end

    it 'should be same when application packages are same on two servers' do
        @diff.compare(@t24, @server1, @server2)

        expect(@diff.is_same).to be_true
    end

    it 'should not be same and return additional packages' do
        pkg = packages(:Package3)
        pkg.application = @t24

        @server1.add_package(pkg)

        expected = [pkg]
        
        @diff.compare(@t24, @server1, @server2)
        
        expect(@diff.is_same).to be_false
        expect(@diff.additional_packages).to eq(expected)
    end

    it 'should not be same and return missed packages' do
        pkg = packages(:Package3)
        pkg.application = @t24

        @server2.add_package(pkg)

        expected = [pkg]
        
        @diff.compare(@t24, @server1, @server2)
        
        expect(@diff.is_same).to be_false
        expect(@diff.missed_packages).to eq(expected)
    end

end
