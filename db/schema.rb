# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20131011050855) do

  create_table "applications", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "production_id"
  end

  add_index "applications", ["name"], :name => "index_applications_on_name", :unique => true

  create_table "applications_servers", :force => true do |t|
    t.integer "server_id"
    t.integer "application_id"
  end

  create_table "deployments", :force => true do |t|
    t.integer  "server_id"
    t.datetime "date"
    t.string   "deployed_by"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "status_id"
  end

  create_table "deployments_packages", :force => true do |t|
    t.integer "deployment_id"
    t.integer "package_id"
  end

  create_table "packages", :force => true do |t|
    t.string   "name"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "application_id"
  end

  create_table "packages_servers", :force => true do |t|
    t.integer "server_id"
    t.integer "package_id"
  end

  create_table "refreshes", :force => true do |t|
    t.integer  "server_id"
    t.integer  "application_id"
    t.datetime "eod_date"
    t.integer  "status_id"
    t.datetime "implement_time"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "source_id"
  end

  add_index "refreshes", ["application_id"], :name => "index_refreshes_on_application_id"
  add_index "refreshes", ["server_id"], :name => "index_refreshes_on_server_id"
  add_index "refreshes", ["status_id"], :name => "index_refreshes_on_status_id"

  create_table "servers", :force => true do |t|
    t.string   "name"
    t.string   "host"
    t.string   "ip"
    t.datetime "created_at",                      :null => false
    t.datetime "updated_at",                      :null => false
    t.boolean  "is_benchmark", :default => false
  end

  create_table "statuses", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

end
