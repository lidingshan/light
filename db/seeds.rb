# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Server.delete_all
Application.delete_all
Package.delete_all
Deployment.delete_all
Refresh.delete_all
Status.delete_all

Status.create(:name=>"Open")
status = Status.create(:name=>"Closed")

sit = Server.create(name: 'SIT', host: 'SITServer', ip: '10.0.0.1')
uat = Server.create(name: 'UAT', host: 'UATServer', ip: '10.0.0.2')
pre_prod = Server.create(name: 'PreProd', host: 'PreProdServer', ip: '10.0.0.3')
prod = Server.create(name: 'Prod', host: 'ProdServer', ip: '10.0.0.4', is_benchmark: true)


t24 = Application.create(name: 'T24')
t24.production = prod
t24.save

fiscal = Application.create(name: 'Fiscal')
fiscal.production = prod
fiscal.save

pkg1 = Package.new(:name=>'PKG_0001')
pkg2 = Package.new(:name=>'PKG_0002')

pkg1.application = t24
pkg2.application = fiscal

pkg1.save
pkg2.save

deploy1 = Deployment.new(
    :date=>'2013-9-1',
    :deployed_by=>'Laksh'
)

deploy2 = Deployment.new(
    :date=>'2013-9-2',
    :deployed_by=>'Mitch'
)

deploy1.status = status
deploy2.status = status

deploy1.packages<<pkg1
deploy1.packages<<pkg2

deploy2.packages<<pkg1
deploy2.packages<<pkg2

deploy1.server = pre_prod 
deploy2.server = prod

deploy1.save
deploy2.save

pre_prod.deployments<<deploy1
prod.deployments<<deploy2

uat.save
prod.save
