class AlterApplicationNameColumnToBeUnique < ActiveRecord::Migration
    def change
        add_index :applications, :name, :unique=>true
    end
end
