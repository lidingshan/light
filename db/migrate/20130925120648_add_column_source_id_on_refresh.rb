class AddColumnSourceIdOnRefresh < ActiveRecord::Migration
  def change
      add_column :refreshes, :source_id, :integer, index: true
  end
end
