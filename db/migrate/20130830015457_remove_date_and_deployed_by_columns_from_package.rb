class RemoveDateAndDeployedByColumnsFromPackage < ActiveRecord::Migration
  def change
      remove_column :packages, :date
      remove_column :packages, :deployed_by
  end
end
