class CreateTableServerPackage < ActiveRecord::Migration
  def change
      create_table :packages_servers do |t|
          t.belongs_to :server
          t.belongs_to :package
      end
  end
end
