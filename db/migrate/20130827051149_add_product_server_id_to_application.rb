class AddProductServerIdToApplication < ActiveRecord::Migration
  def change
      add_column :applications, :prod_server_id, :integer
      add_index :applications, :prod_server_id
  end
end
