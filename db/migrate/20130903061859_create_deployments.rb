class CreateDeployments < ActiveRecord::Migration
  def change
    create_table :deployments do |t|
        t.references :servers, index: true
        t.datetime :date
        t.string :deployed_by
        t.timestamps
    end
  end
end
