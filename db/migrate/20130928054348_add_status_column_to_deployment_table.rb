class AddStatusColumnToDeploymentTable < ActiveRecord::Migration
  def change
      add_column :deployments, :status_id, :integer
  end
end
