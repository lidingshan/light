class AddProductionServerReferenceToApplicationTable < ActiveRecord::Migration
  def change
      add_column :applications, :production_id, :integer
      remove_column :applications, :prod_server_id
  end
end
