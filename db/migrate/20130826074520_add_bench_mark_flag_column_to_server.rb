class AddBenchMarkFlagColumnToServer < ActiveRecord::Migration
  def change
      add_column :servers, :is_benchmark, :boolean, default: false
  end
end
