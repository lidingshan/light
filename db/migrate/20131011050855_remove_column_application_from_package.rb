class RemoveColumnApplicationFromPackage < ActiveRecord::Migration
  def change
    remove_column :packages, :application
  end
end
