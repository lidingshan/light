class ChangeColumnDeploymentServersId < ActiveRecord::Migration
  def change 
      rename_column :deployments, :servers_id, :server_id
  end
end
