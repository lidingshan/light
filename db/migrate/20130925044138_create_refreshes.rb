class CreateRefreshes < ActiveRecord::Migration
  def change
    create_table :refreshes do |t|
      t.references :server
      t.references :application
      t.datetime :eod_date
      t.references :status
      t.datetime :implement_time

      t.timestamps
    end
    add_index :refreshes, :server_id
    add_index :refreshes, :application_id
    add_index :refreshes, :status_id
  end
end
