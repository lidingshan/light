class AddColumnApplicationIdToPackages < ActiveRecord::Migration
  def change
      add_column :packages, :application_id, :integer
  end
end
