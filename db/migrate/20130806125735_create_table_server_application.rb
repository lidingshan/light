class CreateTableServerApplication < ActiveRecord::Migration
  def change
    create_table :applications_servers do |t|
        t.belongs_to :server
        t.belongs_to :application
    end
  end
end
