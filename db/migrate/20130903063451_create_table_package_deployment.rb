class CreateTablePackageDeployment < ActiveRecord::Migration
  def change 
    create_table :deployments_packages do |t|
        t.belongs_to :deployment
        t.belongs_to :package
    end 
  end
end
