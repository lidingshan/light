class CreatePackages < ActiveRecord::Migration
  def change
    create_table :packages do |t|
      t.string :name
      t.string :application
      t.datetime :date
      t.string :deployed_by

      t.timestamps
    end
  end
end
